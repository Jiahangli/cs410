package cs410.src.main.java;

import java.io.*;
import java.sql.*;
import java.util.*;

import com.jcraft.jsch.*;

/**
 * CS410 database project: Twitter System
 * Group 6: Jiahang Li, Kenny Overly, Michael Plaisance
 * Driver Class: create database, schema create and data insertion
 * @author JiahangLi
 * with refernce of the JDBCexample class file
 */
public class CreateDatabase {

		public static void main(String[] args) throws SQLException {
			if (args.length !=3){
				System.out.println("Usage Execute <BroncoUser> <BroncoPassword> <DBname>");
				}
			else{
				Connection con = null;
				Session session = null;
				try
				{
					String strSshUser = args[0];                  	   // SSH loging username
					String strSshPassword = args[1];                   // SSH login password
					String strDBname = args[2];                  	   // Database name

					
					String strSshHost = "onyx.boisestate.edu";         // hostname or ip or SSH server
					int nSshPort = 22;                                 // remote SSH host port number
					String strRemoteHost = "localhost";  			   // hostname or ip of your database server

					int nLocalPort = 3367;  						   // local port number use to bind SSH tunnel
					String strDbUser = "msandbox";                     // database loging username
					String strDbPassword = "123456";                   // database login password
					int nRemotePort = 10167;       					   // remote port number of your database 
					
					/*
					 * STEP 0
					 * CREATE a SSH session to ONYX
					 * 
					 * */
					System.out.println("Attempting to connect to " + strSshUser + "...");
					session = CreateDatabase.doSshTunnel(strSshUser, strSshPassword, strSshHost, nSshPort, strRemoteHost, nLocalPort, nRemotePort);
					
					
					/*
					 * STEP 1 and 2
					 * LOAD the Database DRIVER and obtain a CONNECTION
					 * 
					 * */
					Class.forName("com.mysql.jdbc.Driver");
					con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:"+nLocalPort, strDbUser, strDbPassword);
					System.out.println("Connected database successfully...");


					//STEP 3: create tables
					//STEP 4: LOAD DATA INFILE
					Statement stmt = null;
					stmt = con.createStatement();
					stmt.execute("use "+ strDBname);
				
				
					//User table
					stmt.execute("create table User (id int, username char(100) NOT NULL, location char(100), primary key (id));") ;
					
					stmt.execute("LOAD DATA INFILE '~/workspace/410-Final-Project/tables/user.csv' "+
							"INTO TABLE User FIELDS TERMINATED BY '|' LINES TERMINATED BY '\n';");
					
					
					
					//Tweets table
					stmt.execute("create table Tweets(userid int NOT NULL, tweetid int(6) NOT NULL, time_stamp timestamp,"+
								"tweet char(250), foreign key (userid) references User(id), primary key (tweetid));");
					
					String load = 
							 "LOAD DATA INFILE '~/workspace/410-Final-Project/tables/Tweets.csv' INTO TABLE Tweets FIELDS TERMINATED BY '|' LINES TERMINATED BY '\n';";
					
					stmt.execute(load);
					
					//ReTweet table
					stmt.execute("create table ReTweet(user1 int, user2 int,"
							+"foreign key (user1) references User(id), foreign key(user2) references User(id),"
							+"primary key(user1, user2));");
					
					load = 
							"LOAD DATA INFILE '~/workspace/410-Final-Project/tables/ReTweet.csv' INTO TABLE ReTweet FIELDS TERMINATED BY '|' LINES TERMINATED BY '\n';";
					stmt.execute(load);
					
					//Mention table
					stmt.execute("create table Mention (user1 int, user2 int, foreign key (user1) references User(id),"+
							"foreign key(user2) references User(id),primary key(user1, user2));");
					
					load = 
							"LOAD DATA INFILE '~/workspace/410-Final-Project/tables/Mention.csv' INTO TABLE Mention FIELDS TERMINATED BY '|' LINES TERMINATED BY '\n';";
					stmt.execute(load);
					
					//Reply table
					stmt.execute("create table Reply( user1 int, user2 int,foreign key (user1) references User(id),"+
							"foreign key(user2) references User(id), primary key(user1, user2));");
					
					load = 
							"LOAD DATA INFILE '~/workspace/410-Final-Project/tables/Reply.csv' INTO TABLE Reply FIELDS TERMINATED BY '|' LINES TERMINATED BY '\n';";
					stmt.execute(load);
					
					//Followers table
					stmt.execute("create table Followers(" 
							+"follower int, followee int, foreign key (follower) references User(id),"
							+"foreign key(followee) references User(id), primary key(follower, followee));");

					load = 
							"LOAD DATA INFILE '~/workspace/410-Final-Project/tables/followers.csv' INTO TABLE Followers FIELDS TERMINATED BY '|' LINES TERMINATED BY '\n';";
					stmt.execute(load);
					
					
				}
				catch( Exception e )
				{
					e.printStackTrace();
				}
				finally{
					
					/*
					 * STEP 5
					 * CLOSE CONNECTION AND SSH SESSION
					 * 
					 * */
					
					con.close();
					session.disconnect();
					System.out.println("The database is successfully created.");
				}

			}
		}
		
		private static Session doSshTunnel( String strSshUser, String strSshPassword, String strSshHost, int nSshPort, String strRemoteHost, int nLocalPort, int nRemotePort ) throws JSchException
		{
			/*This is one of the available choices to connect to mysql
			 * If you think you know another way, you can go ahead*/
			
			final JSch jsch = new JSch();
			java.util.Properties configuration = new java.util.Properties();
			configuration.put("StrictHostKeyChecking", "no");

			Session session = jsch.getSession( strSshUser, strSshHost, 22 );
			session.setPassword( strSshPassword );

			session.setConfig(configuration);
			session.connect();
			session.setPortForwardingL(nLocalPort, strRemoteHost, nRemotePort);
			return session;
		}


	}





